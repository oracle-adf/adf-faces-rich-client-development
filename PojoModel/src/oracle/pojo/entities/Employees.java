package oracle.pojo.entities;

import java.io.Serializable;

import java.util.Date;

/**
 * Employees Entity 
 * @author Frank Nimphius
 * @version 11.0
 */

public class Employees implements Serializable{
    
    private Long employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Date hireDate;
    private String jobId;
    private Long salary;
    private Long managerId;
    private Long departmentId;
    
    public Employees() {
    }

    public Employees(Long _id,
                     String _firstName,
                     String _lastName,
                     String _email,
                     String _phoneNumber,
                     Date _hireDate,
                     String _jobId,
                     Long _salary,
                     Long _managerId,
                     Long _departmentId){
        
        employeeId = _id;
        firstName = _firstName;
        lastName = _lastName;
        email = _email;
        phoneNumber = _phoneNumber;
        hireDate = _hireDate;
        jobId = _jobId;
        salary = _salary;
        managerId = _managerId;
        departmentId = _departmentId;
    }

    public void setEmployeeId(Long _employeeId) {
        this.employeeId = _employeeId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setFirstName(String _firstName) {
        this.firstName = _firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastname(String _lastname) {
        this.lastName = _lastname;
    }

    public String getLastname() {
        return lastName;
    }

    public void setEmail(String _email) {
        this.email = _email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhoneNumber(String _phoneNumber) {
        this.phoneNumber = _phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setHireDate(Date _hireDate) {
        this.hireDate = _hireDate;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setJobId(String _jobId) {
        this.jobId = _jobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setSalary(Long _salary) {
        this.salary = _salary;
    }

    public Long getSalary() {
        return salary;
    }

    public void setManagerId(Long _managerId) {
        this.managerId = _managerId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setDepartmentId(Long _departmentId) {
        this.departmentId = _departmentId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }
}
