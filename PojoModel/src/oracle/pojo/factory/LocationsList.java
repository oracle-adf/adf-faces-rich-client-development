package oracle.pojo.factory;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import oracle.pojo.entities.Departments;
import oracle.pojo.entities.Locations;


public class LocationsList implements Serializable{
    ArrayList<Locations> locationsList = null;
    DepartmentsList departmentsList = null;
    public LocationsList(DepartmentsList list) {
        departmentsList = list;
    }

    public List<Locations> getLocationsList (){        
        locationsList = new ArrayList<Locations>();
        Locations location = null;
        location = new Locations(new Long(1400), "2014 Jabberwocky Rd","26192","Southlake","Texas",departmentsList);
        locationsList.add(location);
        location = new Locations(new Long(1500), "2011 Interiors Blvd","99236","South San Francisco","California",departmentsList);
        locationsList.add(location);
        location = new Locations(new Long(1600), "2007 Zagora St","50090","South Brunswick","New Jersey",departmentsList);
        locationsList.add(location);    
        location = new Locations(new Long(1700), "2004 Charade Rd","98199","Seattle","Washington",departmentsList);
        locationsList.add(location);
        location = new Locations(new Long(1800), "147 Spadina Ave","M5V 2L7","Toronto","Ontario",departmentsList);
        locationsList.add(location);
        return locationsList;
    }
    
    public void updateLocations(Locations l) {
        if (locationsList == null) {
            //populate departments
            this.getLocationsList();
        }

        for (Locations loc : locationsList) {
            if (loc.getLocationId().equals(l.getLocationId())) {
                locationsList.remove(loc);
                locationsList.add(l);
                return;
            }
        }
    }
}
