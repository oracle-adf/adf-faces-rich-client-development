package oracle.pojo.factory;

import java.io.Serializable;

import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.List;

import oracle.pojo.entities.Departments;


public class DepartmentsList implements Serializable {

    ArrayList<Departments> departmentsList = null;
    EmployeesList employeesList = null;
    
    public DepartmentsList(EmployeesList list) {
        employeesList = list;
    }

    public List<Departments> getDepartmentsList() {
        if (departmentsList == null) {

            departmentsList = new ArrayList<Departments>();

            Departments department = null;
            department =
                    new Departments(new Long(60), "IT", new Long(103), new Long(1400),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(50), "Shipping", new Long(121),
                                    new Long(1500),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(10), "Administration", new Long(200),
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(30), "Purchasing", new Long(114),
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(90), "Executive", new Long(100),
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(100), "Finance", new Long(108),
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(110), "Accounting", new Long(205),
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(120), "Treasury", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(130), "Corporate Tax", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(140), "Control And Credits", null,
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(150), "Shareholder Services", null,
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(160), "Benefit", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(170), "Manufacturing", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(180), "Construction", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(190), "Contracting", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(200), "Operations", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(210), "IT Support", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(220), "NOC", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(230), "IT Helpdesk", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(240), "Government Sales", null,
                                    new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(250), "Retail Sales", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(260), "Recruiting", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(270), "Payroll", null, new Long(1700),employeesList);
            departmentsList.add(department);
            department =
                    new Departments(new Long(20), "Marketing", new Long(201),
                                    new Long(1800),employeesList);
            departmentsList.add(department);
        }
        return departmentsList;
    }

    public void addDepartment(Departments d) {
        if (departmentsList == null) {
            //populate departments
            this.getDepartmentsList();
        }
        departmentsList.add(d);
    }

    public void removeDepartment(Departments d) {
        if (departmentsList == null) {
            //populate departments
            this.getDepartmentsList();
        }

        for (Departments dept : departmentsList) {
            if (dept.getDepartmentId().equals(d.getDepartmentId())) {
                departmentsList.remove(dept);
                return;
            }
        }
    }

    public void updateDepartment(Departments d) {
        if (departmentsList == null) {
            //populate departments
            this.getDepartmentsList();
        }

        for (Departments dept : departmentsList) {
            if (dept.getDepartmentId().equals(d.getDepartmentId())) {
                departmentsList.remove(dept);
                departmentsList.add(dept);
                return;
            }
        }
    }

    public ArrayList<Departments> getDepartmentsByLocationId(Long locationId) {
        ArrayList<Departments> departmentsByLocation =
            new ArrayList<Departments>();
        if (departmentsList == null) {
            //populate departments
            this.getDepartmentsList();
        }

        for (Departments dept : departmentsList) {
            if (dept.getLocationId().equals(locationId)) {
                departmentsByLocation.add(dept);
            }
        }
        return departmentsByLocation;
    }   
}
