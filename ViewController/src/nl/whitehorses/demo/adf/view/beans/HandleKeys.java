package nl.whitehorses.demo.adf.view.beans;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.ui.pattern.dynamicShell.TabContext;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class HandleKeys {

  public void registerKeyboardMapping(PhaseEvent phaseEvent) {
    if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE) {

      FacesContext facesContext = FacesContext.getCurrentInstance();
      ExtendedRenderKitService extRenderKitService =
        Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
      List<UIComponent> childComponents = facesContext.getViewRoot().getChildren();
      //First child component in an ADF Faces page - and the only child - is af:document
      //Thus no need to parse the child components and check for their component family
      //type
      String id = ((UIComponent)childComponents.get(0)).getClientId(facesContext);

      StringBuffer script = new StringBuffer();
      script.append("window.registerKeyBoardHandler('keyboardToServerNotify','" + id + "')");
      extRenderKitService.addScript(facesContext, script.toString());
    }
  }

  public void handleKeyboardEvent(ClientEvent clientEvent) {

    String keyCode = (String)clientEvent.getParameters().get("keycode");
    System.out.println("handleKeyboardEvent: "+keyCode);
    // The alt+<number> keyboard combination opens the relating ADF UI Shell tab if open
    if (keyCode.equalsIgnoreCase("alt 1") || keyCode.equalsIgnoreCase("alt 2") || keyCode.equalsIgnoreCase("alt 3") ||
        keyCode.equalsIgnoreCase("alt 4") || keyCode.equalsIgnoreCase("alt 5") || keyCode.equalsIgnoreCase("alt 6") ||
        keyCode.equalsIgnoreCase("alt 7") || keyCode.equalsIgnoreCase("alt 8") || keyCode.equalsIgnoreCase("alt 9")) {

      String keyIndexStr = keyCode.substring(keyCode.length() - 1, keyCode.length());
      int keyIndex = Integer.parseInt(keyIndexStr);
      TabContext.getCurrentInstance().setSelectedTabIndex(keyIndex - 1); 
    }
  }
}