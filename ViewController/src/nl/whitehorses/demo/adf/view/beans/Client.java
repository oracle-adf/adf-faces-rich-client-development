package nl.whitehorses.demo.adf.view.beans;

import java.util.Iterator;
import java.util.List;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.jbo.Key;

public class Client {

    public Client() {
    }

  public void goCurrentRow(ClientEvent clientEvent) {
       // Determine which row is currently selected in the RichTable
       RichTable deptTable = (RichTable)clientEvent.getComponent();
       Iterator keys = deptTable.getSelectedRowKeys().iterator();
       while (keys.hasNext()) {
           Key key = (Key)((List)keys.next()).get(0);
           System.out.println(key.getKeyValues()[0]);
       }
   }

}
