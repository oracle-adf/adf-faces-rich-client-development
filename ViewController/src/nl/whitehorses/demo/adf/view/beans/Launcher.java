package nl.whitehorses.demo.adf.view.beans;

import java.util.HashMap;
import java.util.Map;

import javax.faces.event.ActionEvent;

import oracle.ui.pattern.dynamicShell.TabContext;

public class Launcher {
    public Launcher() {
        super();
    }
    
     public void detailActivity(ActionEvent actionEvent)
      {

          Map<String, Object> parameterMap = new HashMap<String, Object>();
          parameterMap.put("id","1");
          
         
        _launchActivity(
          "Detail",
          "/WEB-INF/flows/detail.xml#detail",
          true,
          parameterMap);
      }
    
      public void closeCurrentActivity(ActionEvent actionEvent)
       {
         TabContext tabContext = TabContext.getCurrentInstance();
         int tabIndex = tabContext.getSelectedTabIndex();
         if (tabIndex != -1)
         {
           tabContext.removeTab(tabIndex);
         }
       }
      
         public void currentTabDirty(ActionEvent e)
         {
             /**
             * When called, marks the current tab "dirty". Only at the View level
             * is it possible to mark a tab dirty since the model level does not
             * track to which tab data belongs.
             */
           TabContext tabContext = TabContext.getCurrentInstance();
           tabContext.markCurrentTabDirty(true);
         } 
      
         public void currentTabClean(ActionEvent e)
         {
           TabContext tabContext = TabContext.getCurrentInstance();
           tabContext.markCurrentTabDirty(false);
         }
        
         public void checkState(boolean isDirty) {
           /**
           * Example method to call prior to page navigation to determine if
           * any tab has been marked as dirty. If true, a popup dialog in the
           * current page can be called to warn the user of unsaved data.
           */
           TabContext tabContext = TabContext.getCurrentInstance(); 
           boolean isAnyTabDirty = tabContext.isTagSetDirty();
           if (tabContext != null && isAnyTabDirty) {
               // return and warn user in popup dialog from page
             isDirty = isAnyTabDirty;
             return;
           }
           else {
               isDirty = isAnyTabDirty;
           return;
         }
       }
        
       private void _launchActivity(String title, String taskflowId, boolean newTab, Map<String,Object> parameters)
       {
         try
         {
           if (newTab)
           {
             TabContext.getCurrentInstance().addTab(
               title,
               taskflowId,
               parameters);
           }
           else
           {
             TabContext.getCurrentInstance().addOrSelectTab(
               title,
               taskflowId,
               parameters);
           }
         }
         catch (TabContext.TabOverflowException toe)
         {
           // causes a dialog to be displayed to the user saying that there are
           // too many tabs open - the new tab will not be opened...
           toe.handleDefault(); 
         }
       }
}
