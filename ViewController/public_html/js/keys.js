var keyRegistry = new Array();

keyRegistry[0] = "alt 1";
keyRegistry[1] = "alt 2";
keyRegistry[2] = "alt 3";
keyRegistry[3] = "alt 4";
keyRegistry[4] = "alt 5";
keyRegistry[5] = "alt 6";
keyRegistry[6] = "alt 7";
keyRegistry[7] = "alt 8";
keyRegistry[8] = "alt 9";
keyRegistry[9] = "alt 0";

function registerKeyBoardHandler(serverListener, afdocument) {


  _serverListener = serverListener;
  var document = AdfPage.PAGE.findComponentByAbsoluteId(afdocument);
  _document = document;

  AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "registerKeyBoardHandler!");
  
  for (var i = keyRegistry.length - 1; i >= 0; i--) {
    var keyStroke = AdfKeyStroke.getKeyStrokeFromMarshalledString(keyRegistry[i]);     
    AdfRichUIPeer.registerKeyStroke(document, keyStroke, callBack);
  }
}

function callBack(keyCode) {
  var activeComponentClientId = AdfPage.PAGE.getActiveComponentId();
      
  // Send the marshalled key code to the server listener for the developer
  // To handle the function key in a managed bean method      
  var marshalledKeyCode = keyCode.toMarshalledString();

  // {AdfUIComponent} component Component to queue the custom event on
  // {String} name of serverListener
  // {Object} params a set of parameters to include on the event.
  // {Boolean} immediate whether the custom event is "immediate" - 
  //   which will cause it to be delivered during Apply Request
  //   Values on the server, or not immediate, in which 
  //   case it will be delivered during Invoke Application.
          
  // Note that if one of the keyboard functions is to create ADF 
  // bound rows, immediate must be set to false. There is no
  // option yet for the ClientEvent to be queued for later - 
  // InvokeApplication - on the server. 
  AdfLogger.LOGGER.logMessage(AdfLogger.INFO, "log Key!");

  AdfCustomEvent.queue(_document,
                       _serverListener, 
                       {keycode:marshalledKeyCode, activeComponentClientId:activeComponentClientId}
                       , false); 
 
  // indicate to the client that the key was handled and that there 
  // is no need to pass the event to the browser to handle it
  return true;
}