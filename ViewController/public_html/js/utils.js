  function clearMessagesForComponent(evt){         
    AdfPage.PAGE.clearAllMessages();
    evt.cancel();
  }
  
  function numbersOnly(evt){
     var _keyCode = evt.getKeyCode();
     var _filterField = evt.getCurrentTarget();
     var _oldValue = _filterField.getValue();
     
     AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "key: "+_keyCode);
     //check for characters
     if (_keyCode > 64 && _keyCode < 91){  
       _filterField.setValue(_oldValue);
       evt.cancel();	
     }
  }
  
  function clickRow(evt) {
     var source = evt.getSource();
     AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "clickOnRow");
     AdfCustomEvent.queue( source, "clickOnRow", {}, false); 
  } 
  
   function showPopup(popupId, dialogId, attributeId) {
     return function (evt) {
       evt.cancel();
       var source = evt.getSource();
       
       var popup = source.findComponent(popupId);
       var departmentId = source.getProperty("departmentId");
  
       AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "showPopup for "+departmentId);
  
       popup.show();
       var dialog = popup.findComponent("dialog1");
       dialog.setTitle("Department Id "+departmentId);
       AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "setTitle");
     }
  }

   function setTabIndex( min, max) {
     return function (evt) {
       var minTabIndex = min;
       var maxTabIndex = max;
       
       var keyCodePressed = evt.getKeyCode();
       var keyModifiers = evt.getKeyModifiers();
       var component = evt.getSource();
       var panelForm = component.getParent();

       if ( keyCodePressed == AdfKeyStroke.TAB_KEY ) {
         evt.cancel();
         
         var tabIndex = parseInt(component.getProperty("tabindex"));
         AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "setTabIndex found index: " +tabIndex);

         if ( keyModifiers == AdfKeyStroke.SHIFT_MASK ) {
           // backward tab
           tabIndex = ( tabIndex -1 ) < minTabIndex ? maxTabIndex : tabIndex -1;
           AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "setTabIndex new index: " +tabIndex);
           setComponentFocus( panelForm.getDescendantComponents() , tabIndex);
         } else {
           // forward tab
           tabIndex = ( tabIndex +1 ) > maxTabIndex ? minTabIndex : tabIndex +1;
           AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "setTabIndex 2 new index: " +tabIndex);
           setComponentFocus( panelForm.getDescendantComponents() , tabIndex);
         }
       }
     }
  }  

  function setComponentFocus( childComponentArray , focusIndex)  {
    AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "setComponentFocus" );

    for (indx in childComponentArray ) {
      var comp = childComponentArray[indx];
      if ( comp instanceof AdfRichInputText ||
           comp instanceof AdfRichInputDate ) {
        var compIndx = comp.getProperty("tabindex");
        if ( compIndx == focusIndex) {
          comp.focus();
          AdfLogger.LOGGER.logMessage(AdfLogger.ERROR, "focus on id:" + compIndx);
          return false;
        }
      }
    }
    return false;
  }
  
  